import pytest
import math
import time
from selenium import webdriver
from stepik.automation_python import params


@pytest.fixture(scope="function")
def browser():
    print("\nstart browser for test..")
    browser = webdriver.Chrome()
    yield browser
    print("\nquit browser..")
    browser.quit()


@pytest.mark.parametrize('num', params.num)
def test_guest_should_see_login_link(browser, num):
    link = f'https://stepik.org/lesson/{num}/step/1'
    browser.get(link)
    time.sleep(3)
    elem = math.log(int(time.time()))
    answer = str(elem)
    elem = browser.find_element_by_tag_name('textarea')
    elem.send_keys(answer)
    elem = browser.find_element_by_css_selector('.submit-submission')
    elem.click()
    time.sleep(3)
    elem = browser.find_element_by_class_name('smart-hints__hint')
    correct = elem.text
    assert correct == 'Correct!'
    #The owls are not what they seem! OvO