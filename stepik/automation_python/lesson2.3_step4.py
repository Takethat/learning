from selenium import webdriver
import time
import math
from selenium.webdriver.support.ui import Select


def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))


try:
    link = "http://suninjuly.github.io/alert_accept.html"
    browser = webdriver.Chrome()
    browser.get(link)
    elem = browser.find_element_by_css_selector('button[type="submit"]')
    elem.click()
    confirm = browser.switch_to.alert
    confirm.accept()
    elem = browser.find_element_by_id('input_value')
    elem = elem.text
    x = calc(elem)
    elem = browser.find_element_by_name('text')
    elem.send_keys(x)
    elem = browser.find_element_by_css_selector("button[type='submit']")
    elem.click()
finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()
