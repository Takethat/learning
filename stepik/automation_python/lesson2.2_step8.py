import os
from selenium import webdriver
import time
import math
from selenium.webdriver.support.ui import Select
def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))

import random
try:
    current_dir = os.path.abspath(os.path.dirname(__file__))  # получаем путь к директории текущего исполняемого файла
    file_path = os.path.join(current_dir, 'PyTest_commands.txt')  # добавляем к этому пути имя файла
    print(file_path)
    link = "http://suninjuly.github.io/file_input.html"
    browser = webdriver.Chrome()
    browser.get(link)
    elem = browser.find_element_by_name('firstname')
    elem.send_keys('123')
    elem = browser.find_element_by_name('lastname')
    elem.send_keys('1234')
    elem = browser.find_element_by_name('email')
    elem.send_keys('email')
    elem = browser.find_element_by_css_selector('input[type="file"]')
    elem.send_keys(file_path)
    elem = browser.find_element_by_css_selector('button[type="submit"]')
    elem.click()
finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()

