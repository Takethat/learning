from selenium import webdriver
import time
import math
def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))
import random

try:
    link = "http://suninjuly.github.io/get_attribute.html"
    browser = webdriver.Chrome()
    browser.get(link)
    x_element = browser.find_element_by_id("treasure")
    x = x_element.get_attribute('valuex')
    y = calc(x)
    elem = browser.find_element_by_id("answer")
    elem.send_keys(y)
    elem = browser.find_element_by_id("robotCheckbox")
    elem.click()
    elem = browser.find_element_by_id("robotsRule")
    elem.click()
    elem = browser.find_element_by_css_selector('.btn')
    elem.click()
finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()
