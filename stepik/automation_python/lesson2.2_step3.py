from selenium import webdriver
import time
import math
from selenium.webdriver.support.ui import Select
def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))
import random

try:
    link = "http://suninjuly.github.io/selects1.html"
    browser = webdriver.Chrome()
    browser.get(link)
    elem = browser.find_element_by_id("num1")
    x = elem.text
    elem = browser.find_element_by_id("num2")
    y = elem.text
    summ = int(x) + int(y)
    elem = browser.find_element_by_css_selector("#dropdown")
    elem.click()
    elem=browser.find_element_by_css_selector(f"[value='{summ}']")
    elem.click()
    elem = browser.find_element_by_css_selector('.btn')
    elem.click()
finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()
