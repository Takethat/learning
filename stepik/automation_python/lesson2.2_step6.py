from selenium import webdriver
import time
import random
import math
from selenium.webdriver.support.ui import Select
def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))


try:
    link = "http://suninjuly.github.io/execute_script.html"
    browser = webdriver.Chrome()
    browser.get(link)
    elem = browser.find_element_by_id("input_value")
    x = elem.text
    y = calc(x)
    browser.execute_script("window.scrollBy(0, 100);")
    elem = browser.find_element_by_id("answer")
    elem.send_keys(y)
    elem=browser.find_element_by_id("robotCheckbox")
    elem.click()
    elem = browser.find_element_by_id("robotsRule")
    elem.click()
    elem = browser.find_element_by_css_selector('.btn')
    elem.click()
finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()
