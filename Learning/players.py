# Создание сегмента
players = ['charles', 'martina', 'florence', 'eli']
print(players[0:3])


# Сегмент без начала
players = ['charles', 'martina', 'florence', 'eli']
print(players[:2])

# Сегмент без конца
players = ['charles', 'martina', 'florence', 'eli']
print(players[0:])

# Отицательный элемент возвращает с конца списка
players = ['charles', 'martina', 'florence', 'eli']
print(players[-2:])

# Перебор списка
players = ['charles', 'martina', 'florence', 'eli']
for players in players[:3]:
    print(players.title())