# Сложение чисел
summ = 2+3
print(summ)

# Вычитание
subtraction = 3 -2
print(subtraction)

# Умножение
multiplication = 2 * 3
print(multiplication)

# Деление
division = 3/2
print(division)

# Степень
degree = 3 ** 3
print(degree)

# Вещественные числа(числа с плавующей точкой)
subtraction = 3.1 + 3.1
print(subtraction)

# При делении всегда получаем вещественное число
division = 4/2
print(division)

# Соеденение числа с помощью "_"
numbers = 14_0000_000
print(numbers)

