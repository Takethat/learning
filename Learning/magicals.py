# Пример работы цикла for
magician = ['alice', 'david', 'carolina']
for magician in magician:
    print(magician)

magician = ['alice', 'david', 'carolina']
for magician in magician:
    print(f"{magician.title()},that was a great trick!")
    print(f"I can't wait to see your next trick, {magician.title()}\n")
print("Thank you, everone. That was a great magic show!")
