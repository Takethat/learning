# # Копирование списков
# my_food = ['pizza', 'falafel', 'carrot cake']
# friend_food = my_food[:]
# print("My favorire foods are:")
# print(my_food)
#
# print("\nMy friends favorire foods are:")
# print(friend_food)
#
# # Подтверждение
# my_food = ['pizza', 'falafel', 'carrot cake']
# friend_food = my_food[:]
# my_food.append('cannoile')
# friend_food.append('cake')
#
# print("My favorire foods are:")
# print(my_food)
#
# print("\nMy friends favorire foods are:")
# print(friend_food)
#
# # Упражнениие
# my_food = ['pizza', 'falafel', 'carrot cake']
# print('The irst three items in the listare:')
# print(my_food[0:2])
# print(my_food[2:3])
# print(my_food[:])
#
# my_food = ['pizza', 'falafel', 'carrot cake']
# my_friend = my_food[:]
# my_food.append('cofe')
# my_friend.append('cake')
# print(my_food)
# print(my_friend)

my_food = ['pizza', 'falafel', 'carrot cake']
for my_food in my_food[1:3]:
    print(my_food)
