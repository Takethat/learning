# # Кортежи данных
#
# # dimensions = (200, 500)
# # print(dimensions[0])
# # print(dimensions[1])
#
#
# # Попытк изменения кортежа
# # dimensions = (200, 50)
# # dimensions[0] = 250
# # print(dimensions[0])
#
# # Перебор значений в кортеже
#
# # dimensions = (200, 50)
# # for dimension in dimensions:
# #     print(dimension)
#
# # Замена всего кортеже или переопределения
# dimensions = (200, 50)
# print('Original dimensions:')
# for dimension in dimensions:
#     print(dimension)
#
# dimensions = (400, 1000)
# print("\nModified dimensions:")
# for dimension in dimensions:
#     print(dimension)

# Упражнение

food = ('cake', 'coofe', 'kitkat')
for me in food:
    print(me)

food = ('sneeckers', 'melikeeway')
print('\nMe in modified:')
for me in food:
    print(me)
