# Пример возведения в квадрат списка
squres = []
for value in range(1, 11):
    squre = value ** 2
    squres.append(squre)
print(squres)

# Пример меньшего кода с таким же значеним
squares = []
for value in range(1, 11):
    squares.append(value ** 2)
print(squares)

# Уменьшения генератора списка
squares = [value ** 2 for value in range(1, 11)]
print(squares)

# Упражнение

main = list(range(1, 100000))
print(main)
