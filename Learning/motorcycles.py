# Списки добавление
motorcycles = ['honda', 'yamaha', 'suzuki']
motorcycles.append('ducati')
print(motorcycles)

# Создание пустого списка с добавлением элементов
motorcycles = []
motorcycles.append('ducati')
motorcycles.append('Honda')
motorcycles.append('suzuki')
print(motorcycles)

# Добавление в список под определёным номером
motorcycles = ['honda', 'yamaha']
motorcycles.insert(1, 'toyta')
print(motorcycles)

# Удаление из списка по номеру
motorcycles = ['honda', 'yamaha']
del motorcycles[0]
print(motorcycles)

# Удаление из списка по значению
motorcycles = ['honda', 'yamaha']
motorcycles.remove('honda')
print(motorcycles)

# Удаление из списка метод pop(удаление из списка и использование удалённой переменной)
motorcycles = ['honda', 'yamaha', 'suzuki']
popped_motorcycles = motorcycles.pop()
print(motorcycles)
print(popped_motorcycles)

# Вывод последней переменной из списка
motorcycles = ['honda', 'yamaha', 'suzuki']
last_owned = motorcycles.pop()
print(f'The last motorcycles I owned {last_owned}!')

# Использование метода pop для удаления позиции по номеру в списке
motorcycles = ['honda', 'yamaha', 'suzuki']
first_owned = motorcycles.pop(1)
print(f'The last motorcycles I owned {first_owned}!')

# Упражнение
guests = ['dmitri', 'ylia', 'alex', 'sasha']
print(f'Go to my party friend {guests[0].title()}')
guests.insert(1, 'gena')
print(f'Go to my party friend {guests[1].title()}')
guests.insert(2, 'olga')
print(guests)
guests.insert(6, 'Tane')
print(guests)
print(f'Go to my party friend {guests[2].title()}')
print(f'Go to my party friend {guests[3].title()}')
print(f'Go to my parte friend {guests[4].title()}')
delete = guests.pop(1)
print(f'Sorry my dear friend {delete.title()}')
print(guests)

