# Пример f строки
first_name = 'ada'
last_name = 'lovelace'
full_name = f"{first_name} {last_name}"
message = f" Hello, {full_name.title()}!"
print(message)

# Пример табуляции в строках
print('\tName')


# Пример многократной табуляции текста в равноменый столб
print('Language:\n\tPython \n\tC \n\tJavaScript')

# Пример первого слова без табуляции а остальные с табуляцией
print('Language:\nPython \nC \nJavaScript')

# Удаленя пробелов с правой стороны
favorite_language = 'Python '
print(favorite_language.rstrip())

# Удаление на всегда
favorite_language = 'Python '
favorite_language = favorite_language.rstrip()
print(favorite_language)

# Удаление пробелов с левой стороны
favorite_language = ' Python'
favorite_language = favorite_language.lstrip()
print(favorite_language)

