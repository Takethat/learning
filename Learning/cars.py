# Постоянная сортировка списков (по алфавиту)
cars = ['bmw', 'aydi', 'toyta', 'subary']
cars.sort()
print(cars)

# Постоянная сортировка в обратно порядке
cars = ['bmw', 'aydi', 'toyta', 'subary']
cars.sort(reverse=True)
print(cars)

# Временная сортировка списка
cars = ['bmw', 'aydi', 'toyta', 'subary']
print("Here is the original list:")
print(cars)

print("\nHere is sorted list:")
print(sorted(cars))

print("\nHere is the origina list again:")
print(cars)

# Обратный хронологический параметр(просто использование реверса)
cars = ['bmw', 'aydi', 'toyta', 'subary']
cars.reverse()
print(cars)

# Длинна списка
cars = ['bmw', 'aydi', 'toyta', 'subary']
len(cars)

# Упражнение

world = ['japan', 'belarus', 'poland']
print(sorted(world))

print(world)
world.reverse()
print(world)
world = ['japan', 'belarus', 'poland']
world.sort()
print(world)