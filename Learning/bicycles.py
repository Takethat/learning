# Примеры использования списков
bicycles = ['trek', 'cannondale', 'redline', 'specialized']
print(bicycles)
# Пример обращения к списка
print(bicycles[0])

# Прмер с заглавными буквами
print(bicycles[0].title())

# Пример обращения к списка
print(bicycles[-1])

# Пример F строки и списков
print(f'My first bicycles {bicycles[0].title()}')

# Строки свои
names = ['ylia', 'dmitri', 'alex', 'sasha']
print(names[0])
print(names[1])
print(names[2])
print(names[3])
print(f'Hello my dear friend {names[0]}')
print(f'Hello my dear friend {names[1]}')
print(f'Hello my dear friend {names[2]}')
len(names)