# Пример множественного слияния
x, y, z = 1, 2, 3
print(x, y, z)

# Пример выбора констаты
MAX_CONNECTIONS = 5000
print(5+3)
print(16/2)
print(4*2)
print(24-16)

print(f'My favorite number is {MAX_CONNECTIONS}')
