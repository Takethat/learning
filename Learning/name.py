# Пример верхнего регистра
name = 'ada lovelace'
print(name.title())
# Пример всех заглавных
print(name.upper())
# Пример всех нижнего регистра
print(name.lower())

